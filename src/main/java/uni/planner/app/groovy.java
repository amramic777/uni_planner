package uni.planner.app;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
class ThisWillActuallyRun {

    @GetMapping("/Hello")
    String home() {
        return "Hello, World!";
    }

}